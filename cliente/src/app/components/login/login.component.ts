import { Component, OnInit } from '@angular/core';
import {Logins} from "../../models/dbData";
import {AuthService} from "../../service/auth.service";
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario:any
  pass:any
  constructor(
    private auth    : AuthService,
    private router  : Router
    ) {}

  ngOnInit(): void {
  }

  sesion(){
    let nuevoAr:Logins= { username: this.usuario, password: this.pass}
    this.auth.Login(nuevoAr).subscribe(
      data => {
        let datos;
        if (data.status == 1) {
          datos = JSON.stringify(data)
          sessionStorage.setItem('User', datos);
          alert('Bienvenido... ' + data.user);
          this.router.navigateByUrl('/home');
        } else {
          alert('Usuario o contraseña incorrectos')
          this.usuario = ''
          this.pass = ''
        }
      }
    )
  }

}
