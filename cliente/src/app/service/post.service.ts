import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Publicacion } from '../models/dbData';
import { api } from '../models/dbConn';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
  };

  constructor(private httpclient: HttpClient) {}

  create(post: Publicacion): Observable<any> {
    return this.httpclient
      .post<any>(`${api.API_URI}${api.POSTEAR}`, post, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  getAll(): Observable<any> {
    return this.httpclient
      .get<any>(`${api.API_URI}${api.POSTS}`, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    /*if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }*/
    return throwError('Something bad happened; please try again later.');
  }
}
