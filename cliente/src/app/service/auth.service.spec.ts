import {inject, TestBed} from '@angular/core/testing';
import { AuthService } from './auth.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HttpClient, HttpErrorResponse, HttpEvent, HttpEventType, HttpHeaders} from "@angular/common/http";
import {Logins, Usuario1, Usuario2} from "../models/dbData";
import {api} from "../models/dbConn";
import {catchError} from "rxjs/operators";

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [AuthService]
    });
    service = TestBed.inject(AuthService);
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('Constructor()', () => {
    const services: AuthService = TestBed.get(AuthService);
    expect(services).toBeTruthy();
  });

  it('Login()', () => {

    //Arrange
    //Set Up Data
    const login:Logins = { username: 'Jose', password: '201612331' }
    const result = { 'status': 1, 'id': 1, 'user': 'Jose' }

    //Act
    service.Login(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user).toEqual(result);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI+ api.LOGIN);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();

  });

  it('Register()', () => {

    //Arrange
    //Set Up Data
    const login: Usuario1 = { username: 'Jose1',fullname: 'Jose Wannan', photo: '', password: '201612331' }
    const result = { 'status': 1}

    //Act
    service.Create(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user).toEqual(result);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.REGISTRO);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();

  });

  it('getUser()',() =>{
    //Arrange
    //Set Up Data
    //Act
    service.getUsers().subscribe(
      (user)=>
      {
        //Assert-1
        expect(user).length > 1;
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.USERS);
    expect(req.request.method).toBe('GET');
    //Assert -3
    httpMock.verify();
  });

  it('Update()',() =>{
    //Arrange
    //Set Up Data
    const login: Usuario2 = { username: 'Jose1',fullname: 'Jose Orlando Wannan Escobar', photo: 'fotografia', uid: 2}
    const result = { 'status': 1}

    //Act
    service.Update(login).subscribe(
      (user)=>
      {
        //Assert-1
        expect(user).toEqual(result);
      }
    );

    //Assert -2
    const req = httpMock.expectOne(api.API_URI + api.UPDATE);
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();
  });

  it('throws 404 error', () => {
    service.handleError({
      headers: undefined,
      message: "",
      name: "HttpErrorResponse",
      ok: false,
      status: 0,
      statusText: "",
      type: undefined,
      url: undefined,
      error:'ErrorEvent'}).subscribe(
      data => fail('Should have failed with 404 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(undefined);
      }
    );

    const req = httpMock.expectNone(api.API_URI + '/Nadita');

  });

  afterEach(()=>{
    httpMock.verify();
  });


});
