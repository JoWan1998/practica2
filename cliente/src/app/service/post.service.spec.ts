import { TestBed } from '@angular/core/testing';
import { PostService } from './post.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Publicacion } from '../models/dbData';
import { api } from '../models/dbConn';

describe('PostService', () => {
  let service: PostService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PostService],
    });
    service = TestBed.inject(PostService);
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    const services: PostService = TestBed.get(PostService);
    expect(services).toBeTruthy();
  });

  it('create() should call http Post method for the given route', () => {
    // ARRANGE
    // SET UP DATA
    const data: Publicacion = {
      Foto: 'miFoto.jpg',
      Contenido: 'Lorem ipsum',
      ID_Usuario: 1,
    };
    const result = { status: 1 };
    // ACT
    service.create(data).subscribe((post) => {
      expect(post).toEqual(result);
    });
    //Assert -2
    const req = httpMock.expectOne(
      (req) =>
        req.method == 'POST' && req.url === `${api.API_URI}${api.POSTEAR}`
    );
    expect(req.request.method).toBe('POST');
    req.flush(result);
    //Assert -3
    httpMock.verify();
  });

  it('getAll() should call http Get method for the given route', () => {
    // ARRANGE
    // SET TUP DATA
    const result = {
      status: 1,
      post: [
        {
          Contenido: 'Lorem ipsum',
          Foto: 'miFoto.jpg',
          ID_Usuario: 1,
          Fecha: 'Mon, 15/03/21 04:51:00 GTM',
        },
      ],
    };

    // ACT
    service.getAll().subscribe((post) => {
      expect(post).toEqual(result);
    });
    //Assert -2
    const req = httpMock.expectOne(
      (req) => req.method == 'GET' && req.url === `${api.API_URI}${api.POSTS}`
    );
    expect(req.request.method).toBe('GET');
    req.flush(result);
    //Assert -3
    httpMock.verify();
  });

  it('throws 404 error', () => {
    service.handleError({
      headers: undefined,
      message: "",
      name: "HttpErrorResponse",
      ok: false,
      status: 0,
      statusText: "",
      type: undefined,
      url: undefined,
      error:'ErrorEvent'}).subscribe(
      data => fail('Should have failed with 404 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(undefined);
      }
    );

    const req = httpMock.expectNone(api.API_URI + '/Nadita');

  });

  afterEach(() => {
    httpMock.verify();
  });

});
