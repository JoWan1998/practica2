import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ModificacionComponent } from './pages/modificacion/modificacion.component';
import { PostComponent } from './pages/post/post.component';
import {RegisterComponent} from "./pages/register/register.component";

const routes: Routes = [
  { path:'', component:LoginComponent },
  { path: 'Register', component: RegisterComponent},
  { path: 'Modification', component: ModificacionComponent},
  { path: 'home', component: PostComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
