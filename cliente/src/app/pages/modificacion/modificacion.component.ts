import { Component, OnInit } from '@angular/core';
import {Usuario2} from "../../models/dbData";
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-modificacion',
  templateUrl: './modificacion.component.html',
  styleUrls: ['./modificacion.component.css']
})
export class ModificacionComponent implements OnInit {

  usuario: any;
  nombre: any;
  src: any;
  contra: any;

  usuarioActual: any;
  nombreActual: any;
  srcActual: any;
  contraActual: any;

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.getUsers().subscribe(
      data => {
        for(let i = 0; i < data.lenght; i++) {
          if (data[i].Usuario == sessionStorage.user.user){
            this.contraActual = data[i].Contrasenia;
            this.srcActual = data[i].Foto;
            this.usuarioActual = data[i].Usuario;
            this.nombreActual = data[i].Nombre;
            break;
          }
        }
      }
    )
  }

  fileChange(element): void { 
    let files: Array<File> = element.target.files;
    if(files && files.length) {
      let reader = new FileReader();
      reader.readAsDataURL(files[0]);
      reader.onload  = () => {
        this.src = reader.result;
      }
    }
  }

  Modificar() {
    let usuarioLocal = sessionStorage.user;

    let modificacion:Usuario2 = {username: this.usuario, fullname: this.nombre, photo: this.src, uid:  Number(usuarioLocal.id)};
    
    if(this.contra == this.contraActual) {
      this.auth.Update(modificacion).subscribe(
        data => {
          if (data.status == 1) {
            alert('Datos modificados con exito');
            usuarioLocal.user = this.usuario
            sessionStorage.clear()
            sessionStorage.setItem("user", usuarioLocal);
          }else {
            alert('Los datos no han sido modificados');
          }
        }
      )
    }else {
      alert("La contraseña ingresada no es la correcta");
    }
  }

}
