import { Component, OnInit } from '@angular/core';
import {Logins, Usuario, Usuario1} from "../../models/dbData";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  usuario   : any;
  password  : any;
  nombre    : any;
  src       : any;
  photo     : File;

  constructor(
      private auth: AuthService,
      private router: Router
  ) {
    this.src = "../../../assets/images/register1.png";
    this.usuario = "";
    this.password = "";
    this.nombre = "";
   }

  ngOnInit(): void {
  }

  Create(): void {
    if(this.photo == null){
      alert("No se ha seleccionado ninguna foto.");
      return;
    } else if(this.usuario == "" || this.password == "" || this.nombre == "") {
      return;
    }

    let base64 = this.src.split(',')[1];
    let vals:Usuario1 = {
      username: this.usuario,
      fullname: this.nombre,
      password: this.password,
      photo: base64
    }

    this.auth.getUsers().subscribe(
      (res) => {
        if(res.status == 1) {
          let users = res.users;
          for(let i = 0; i < users.length; i++) {
            let user = users[i];
            if(user[1] == this.usuario) {
              alert('Ya existe el usuario: '+ this.usuario)
              return;
            }
          }
          this.auth.Create(vals).subscribe(
            (data) => {
              if(data.status != -1) {
                alert('Usuario Creado');
                this.router.navigate(['']);
              } else {
                alert('No se puede crear el usuario');
                this.usuario = ''
                this.nombre = ''
                this.password = ''
              }
            }, (err) => console.log(err)
          );
        } else {
          alert('Error en el servidor.');
        }
      }, (err) => console.log(err)
    );
  }

  fileChange(element): void
  {
    let files: Array<File> = element.target.files;
    if(files && files.length) {
      this.photo = files[0];
      let reader = new FileReader();
      reader.readAsDataURL(this.photo);
      reader.onload = () => {
        this.src = reader.result;
      };
    }
  }
}

