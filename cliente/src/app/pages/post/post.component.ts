import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from "../../service/post.service";
import { Publicacion } from "../../models/dbData";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  publications  : any;
  src           : any;
  photo         : File;
  content       : string;
  user          : any;
  postList      : Array<Publicacion>;

  constructor(
    private posts : PostService,
    private router  : Router
  ) {
    this.src = "../../../assets/images/register1.png";
    this.content = "";
    this.user = null;
    this.photo = null;
    this.postList = [];
   }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('User'));
    if(this.user == null) {
      this.router.navigateByUrl('/');
    }else {
      this.getPosts();
    }
  }

  getPosts(): void {
    this.postList = [];
    this.posts.getAll().subscribe(
      (res) => {
        console.log(res);
        if(res.status != null && res.status == 1) {
          this.postList = res.posts;
        } else {
          alert("Error interno del servidor.");
        }
      }, (err) => console.log(err)
    );
  }

  createPost(): void {
    let base64 = this.photo == null ? "" : this.src.split(',')[1];
    let name = base64 == "" ? "" : this.photo.name.split('.')[0];
    let post: Publicacion = {
      Contenido   : this.content,
      Foto        : base64,
      ID_Usuario  : this.user.id,
      Nombre_foto : name,
      Fecha       : new Date(),
      Usuario     : this.user.user
    };
    
    if(post.Contenido == "" && post.Foto == "") {
      alert("No se ha ingresado contenido para publicar.");
      return;
    }

    this.posts.create(post).subscribe(
      (res) => {
        if(res.status == 1) {
          alert("¡Publicacion creada exitosamente!");
        } else {
          alert("¡Error al publicar!")
        }
      }, (err) => console.log(err)
    );
    console.log(this.user);
    this.getPosts();
  }

  fileChange(element): void
  {
    let files: Array<File> = element.target.files;
    if(files && files.length) {
      this.photo = files[0];
      let reader = new FileReader();
      reader.readAsDataURL(this.photo);
      reader.onload = () => {
        this.src = reader.result;
      };
    }
  }
}
