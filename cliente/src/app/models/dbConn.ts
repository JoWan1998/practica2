export class db {
  static user: string = 'root';
  static pass: string = 'root';
  static db: string = 'practica2';
}

export class api {
  static API_URI: string = 'http://127.0.0.1:5000';
  static LOGIN: string = '/user/login';
  static REGISTRO: string = '/user/create';
  static POSTEAR: string = '/post/create';
  static POSTS: string = '/post';
  static UPDATE: string = '/user/update';
  static USERS: string = '/users';
}
