export interface Usuario {
  ID_Usuario?: number;
  Usuario?: number;
  Nombre?: string;
  Foto?: string;
  Contrasenia?: string;
}

export interface Usuario1 {
  username?: string;
  fullname?: string;
  photo?: string;
  password?: string;
}

export interface Usuario2 {
  username?: string;
  fullname?: string;
  photo?: string;
  uid?: number;
}

export interface Publicacion {
  Contenido?: string;
  Foto?: string;
  ID_Usuario?: number;
  Nombre_foto?: string;
  Fecha?: Date;
  Usuario?: string;
}

export interface Logins {
  username?: string;
  password?: string;
}
