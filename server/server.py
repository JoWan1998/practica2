from flask import Flask, jsonify, request
from flask_cors import CORS
import json
import mysql.connector
import base64
from datetime import datetime

app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

db_creds = {
    "user"      : "root",
    "password"  : "root",
    "host"      : "localhost",
    "database"  : "practica2"
}

path = '../cliente/src/assets/images/uploads/'

def uploadImage(name, image):
    url_image = path + name + datetime.now().strftime('%m%Y%H%M') + '.jpg'
    with open(url_image, 'wb') as f:
        f.write(base64.b64decode(image))
    return url_image

def uploadImage_post(name, image):
    url_image = path + name
    with open(url_image, 'wb') as f:
        f.write(base64.b64decode(image))
    return url_image

@app.route('/', methods = ['GET'])
def init():
    return "Server working correctly."


@app.route('/users', methods= ['GET'])
def getUsers():
    try:  
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        loginScript = "SELECT * FROM Usuario"
        cursor.execute(loginScript)
        result = cursor.fetchall()
        db.close()
        return jsonify({
            "status"    : 1,
            "users"     : result
        })
    except:
        return jsonify({"status": 0})


@app.route('/user/create', methods = ['POST'])
def createUser():
    try:
        data = request.get_json()
        username = data["username"]
        fullname = data["fullname"]
        password = data["password"]

        url_image = uploadImage(data['username'], data['photo'])

        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        insertScript = "INSERT INTO Usuario (Usuario, Nombre, Foto, Contrasenia) VALUES (%s, %s, %s, %s)"
        cursor.execute(insertScript, (username, fullname, url_image, password))
        db.commit()
        cursor.close()
        db.close()
        print(cursor.rowcount, "record inserted.")
        return jsonify({"status": 1})
    except:
        return jsonify({"status": 0})


@app.route('/user/update', methods = ['POST'])
def updateUser():
    try:
        data = request.get_json()
        username = data["username"]
        fullname = data["fullname"]
        uid = data["id"]

        url_image = uploadImage(data['username'], data['photo'])

        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        updateScript = "UPDATE Usuario AS u SET u.Usuario = %s, u.Nombre = %s, u.Foto = %s WHERE u.ID_Usuario = %s"
        cursor.execute(updateScript, (username, fullname, url_image, uid))
        db.commit()
        cursor.close()
        db.close()
        print(cursor.rowcount, "record updated.")
        return jsonify({"status": 1})
    except:
        return jsonify({"status": 0})


@app.route('/user/login', methods = ['POST'])
def loginUser():
    try:  
        data = request.get_json()
        username = data["username"]
        password = data["password"]
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        cursor1 = db.cursor()
        loginScript = "SELECT COUNT(*) FROM Usuario AS u WHERE u.Usuario = %s AND u.Contrasenia = %s"
        cursor.execute(loginScript, (username, password))
        result = cursor.fetchall()[0][0]
        if result == 1:
            loginScript1 = "SELECT ID_Usuario, Usuario FROM Usuario AS u WHERE u.Usuario = %s AND u.Contrasenia = %s"
            cursor1.execute(loginScript1, (username, password))
            result1 = cursor1.fetchall()[0]            
        db.close()
        return jsonify({
            "status" : 1,
            "id"     : result1[0],
            "user"   : result1[1]
        })         
        
    except:
        return jsonify({"status": 0})

@app.route('/post/create', methods=['POST'])
def createPost():
    try:
        data = json.loads(request.data)
        name = data['Nombre_foto'] + datetime.now().strftime('%m%Y%H%M') + '.jpg'
        url_image = uploadImage_post(name, data['Foto'])

        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        query = """INSERT INTO Publicacion
                    (Contenido, Foto, Id_Usuario)
                    VALUES (%s, %s, %s)"""
        values = (data['Contenido'], name, data['ID_Usuario'])
        cursor.execute(query, values)
        db.commit()
        db.close()
        return jsonify({"status": 1})
    except:
        return jsonify({"status":0})

@app.route('/post', methods=['GET'])
def getPosts():
    postArray = []
    try:
        db = mysql.connector.connect(**db_creds)
        cursor = db.cursor()
        query = """SELECT 
                    P.Contenido, 
                    P.Foto, 
                    P.ID_Usuario, 
                    P.Fecha, 
                    U.Usuario
                    FROM Publicacion P, Usuario U
                    WHERE P.ID_Usuario = U.ID_Usuario 
                    ORDER BY P.Fecha DESC"""
        cursor.execute(query)
        results = cursor.fetchall()
        if cursor.rowcount > 0:
            for result in results:
                post = {
                    "Contenido": result[0],
                    "Foto": result[1],
                    "ID_Usuario": result[2],
                    "Fecha": result[3],
                    "Usuario": result[4]
                }
                postArray.append(post)

        db.close();

        return jsonify({
            "status": 1,
            "posts": postArray
        })
    except:
        return jsonify({"status": 0, "posts": postArray})

if __name__ == "__main__":
    app.run(debug = True)